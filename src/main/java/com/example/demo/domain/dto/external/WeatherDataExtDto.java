package com.example.demo.domain.dto.external;


import com.fasterxml.jackson.annotation.JsonProperty;

public class WeatherDataExtDto {
    private CoordExtDto coord;
    private WeatherExtDto[] weather;
    private MainExtDto main;
    private int visibility;
    private WindExtDto wind;
    private CloudsExtDto clouds;
    private int dt;
    @JsonProperty("dt_txt")
    private String dtTxt;

    public CoordExtDto getCoord() {
        return coord;
    }

    public void setCoord(CoordExtDto coord) {
        this.coord = coord;
    }

    public WeatherExtDto[] getWeather() {
        return weather;
    }

    public void setWeather(WeatherExtDto[] weather) {
        this.weather = weather;
    }

    public MainExtDto getMain() {
        return main;
    }

    public void setMain(MainExtDto main) {
        this.main = main;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public WindExtDto getWind() {
        return wind;
    }

    public void setWind(WindExtDto wind) {
        this.wind = wind;
    }

    public CloudsExtDto getClouds() {
        return clouds;
    }

    public void setClouds(CloudsExtDto clouds) {
        this.clouds = clouds;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

}
