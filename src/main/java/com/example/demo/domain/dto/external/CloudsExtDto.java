package com.example.demo.domain.dto.external;

public class CloudsExtDto {
    private int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
