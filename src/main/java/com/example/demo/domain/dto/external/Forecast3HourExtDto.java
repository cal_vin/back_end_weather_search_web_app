package com.example.demo.domain.dto.external;

import java.util.HashMap;

public class Forecast3HourExtDto {
    private int cnt;
    private WeatherDataExtDto[] list;

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public WeatherDataExtDto[] getList() {
        return list;
    }

    public void setList(WeatherDataExtDto[] list) {
        this.list = list;
    }

    public HashMap<String, String> toWeatherMap() {
        HashMap<String, String> forecastMap = new HashMap<String, String>();
        forecastMap.put(this.list[0].getDtTxt(), this.list[0].getWeather()[0].getDescription());
        forecastMap.put(this.list[1].getDtTxt(), this.list[1].getWeather()[0].getDescription());
        forecastMap.put(this.list[2].getDtTxt(), this.list[2].getWeather()[0].getDescription());
        return forecastMap;
    }
}
