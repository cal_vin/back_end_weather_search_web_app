package com.example.demo.domain.dto.external;

public class CoordExtDto {
    private Double lon;
    private Double lat;
    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }
}
