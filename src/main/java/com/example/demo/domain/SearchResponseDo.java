package com.example.demo.domain;

import java.util.HashMap;

public class SearchResponseDo {
    private Double lon;
    private Double lat;
    private String description;
    private Double temp;
    private Double tempMin;
    private Double tempMax;
    private int humidity;
    private HashMap<String,String> forecast;

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLon() {
        return lon;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLat() {
        return lat;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTempMin(Double tempMin) {
        this.tempMin = tempMin;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public void setTempMax(Double tempMax) {
        this.tempMax = tempMax;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setForecast(HashMap<String, String> forecast) {
        this.forecast = forecast;
    }

    public HashMap<String, String> getForecast() {
        return forecast;
    }
}
