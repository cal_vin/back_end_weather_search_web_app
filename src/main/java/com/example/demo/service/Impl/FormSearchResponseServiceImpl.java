package com.example.demo.service.Impl;

import com.example.demo.domain.SearchResponseDo;
import com.example.demo.domain.dto.external.Forecast3HourExtDto;
import com.example.demo.domain.dto.external.WeatherDataExtDto;
import com.example.demo.service.FormSearchResponseService;
import org.springframework.stereotype.Service;

@Service
public class FormSearchResponseServiceImpl implements FormSearchResponseService {
    public SearchResponseDo formSearchResponse(WeatherDataExtDto weatherData, Forecast3HourExtDto forecast) {
      SearchResponseDo responseDo = new SearchResponseDo();
      responseDo.setLon(weatherData.getCoord().getLon());
      responseDo.setLat(weatherData.getCoord().getLat());
      responseDo.setDescription(weatherData.getWeather()[0].getDescription());
      responseDo.setTemp(weatherData.getMain().getTemp());
      responseDo.setTempMin(weatherData.getMain().getTempMin());
      responseDo.setTempMax(weatherData.getMain().getTempMax());
      responseDo.setHumidity(weatherData.getMain().getHumidity());
      responseDo.setForecast(forecast.toWeatherMap());

      return responseDo;
    };


}
