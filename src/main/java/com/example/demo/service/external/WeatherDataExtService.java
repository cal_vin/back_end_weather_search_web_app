package com.example.demo.service.external;

import com.example.demo.domain.dto.external.WeatherDataExtDto;

public interface WeatherDataExtService {
    public WeatherDataExtDto getWeatherData(Double lat, Double lon);
}
