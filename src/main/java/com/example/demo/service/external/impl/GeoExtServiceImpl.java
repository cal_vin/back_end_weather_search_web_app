package com.example.demo.service.external.impl;

import com.example.demo.domain.dto.external.GeoExtDto;
import com.example.demo.service.external.GeoExtService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@JsonIgnoreProperties(ignoreUnknown = true)
@Service
public class GeoExtServiceImpl implements GeoExtService {
    private RestTemplate restTemplate = new RestTemplate();

    public GeoExtDto[] getGeo(String cityName){

        ResponseEntity<GeoExtDto[]> response = restTemplate.getForEntity(
                "http://api.openweathermap.org/geo/1.0/direct?q="+cityName+"&limit=5&appid=9c8f5a888d7badacd37984276f2168d2",
                GeoExtDto[].class
        );

        GeoExtDto[] geoExtDtos= response.getBody();

        return geoExtDtos;
    };
}
