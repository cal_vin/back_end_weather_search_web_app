package com.example.demo.service.external.impl;

import com.example.demo.domain.dto.external.WeatherDataExtDto;
import com.example.demo.service.external.WeatherDataExtService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
@JsonIgnoreProperties(ignoreUnknown = true)
@Service
public class WeatherDataExtServiceImpl implements WeatherDataExtService {

    private RestTemplate restTemplate = new RestTemplate();

    public WeatherDataExtDto getWeatherData(Double lat, Double lon) {
        WeatherDataExtDto weatherDataExtDto = restTemplate.getForObject(
                "https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&units=metric&appid=9c8f5a888d7badacd37984276f2168d2",
                WeatherDataExtDto.class
        );

        return weatherDataExtDto;
    }

}
