package com.example.demo.service.external.impl;

import com.example.demo.domain.dto.external.Forecast3HourExtDto;
import com.example.demo.service.external.Forecast3HourExtService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@JsonIgnoreProperties(ignoreUnknown = true)
@Service
public class Forecast3HourExtServiceImpl implements Forecast3HourExtService {
    private RestTemplate restTemplate = new RestTemplate();

    public Forecast3HourExtDto getForecasts(Double lat, Double lon){
        Forecast3HourExtDto forecast3HourExtDto = restTemplate.getForObject(
                "https://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lon+"&cnt=3&units=metric&appid=9c8f5a888d7badacd37984276f2168d2",
                Forecast3HourExtDto.class
        );
        return forecast3HourExtDto;
    };

}
