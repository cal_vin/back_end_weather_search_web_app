package com.example.demo.service.external;

import com.example.demo.domain.dto.external.Forecast3HourExtDto;

public interface Forecast3HourExtService {
    public Forecast3HourExtDto getForecasts(Double lat, Double lon);
}
