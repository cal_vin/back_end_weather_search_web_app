package com.example.demo.service.external;

import com.example.demo.domain.dto.external.GeoExtDto;
import org.springframework.http.ResponseEntity;

public interface GeoExtService {
    public GeoExtDto[] getGeo(String cityName);
}
