package com.example.demo.service;

import com.example.demo.domain.SearchResponseDo;
import com.example.demo.domain.dto.external.Forecast3HourExtDto;
import com.example.demo.domain.dto.external.WeatherDataExtDto;

public interface FormSearchResponseService {
    public SearchResponseDo formSearchResponse(WeatherDataExtDto weatherData, Forecast3HourExtDto forecast);
}
