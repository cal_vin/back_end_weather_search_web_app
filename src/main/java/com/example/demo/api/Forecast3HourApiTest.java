package com.example.demo.api;

import com.example.demo.domain.dto.external.Forecast3HourExtDto;
import com.example.demo.service.external.impl.Forecast3HourExtServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Forecast3HourApiTest {
    @Autowired
    Forecast3HourExtServiceImpl forecast3HourExtService;

    @GetMapping("/forecast/test/{lat}/{lon}")
    public Forecast3HourExtDto getForecasts(
            @PathVariable("lat") Double lat,
            @PathVariable("lon") Double lon
    ) {
        return forecast3HourExtService.getForecasts(lat, lon);
    }
}
