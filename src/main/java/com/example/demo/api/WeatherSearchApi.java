package com.example.demo.api;

import com.example.demo.domain.SearchResponseDo;
import com.example.demo.domain.dto.external.Forecast3HourExtDto;
import com.example.demo.domain.dto.external.GeoExtDto;
import com.example.demo.domain.dto.external.WeatherDataExtDto;
import com.example.demo.domain.dto.external.WeatherExtDto;
import com.example.demo.service.FormSearchResponseService;
import com.example.demo.service.Impl.FormSearchResponseServiceImpl;
import com.example.demo.service.external.impl.Forecast3HourExtServiceImpl;
import com.example.demo.service.external.impl.GeoExtServiceImpl;
import com.example.demo.service.external.impl.WeatherDataExtServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherSearchApi {
    @Autowired
    GeoExtServiceImpl geoExtService;
    @Autowired
    WeatherDataExtServiceImpl weatherDataExtService;
    @Autowired
    Forecast3HourExtServiceImpl forecast3HourExtService;
    @Autowired
    FormSearchResponseServiceImpl formSearchResponseService;

    @GetMapping("/weatherSearch/{cityName}")
    public SearchResponseDo searchWeather(
            @PathVariable("cityName") String cityName
    ) {
        GeoExtDto[] geoExtDtos = geoExtService.getGeo(cityName);
        Double lat = geoExtDtos[0].getLat();
        Double lon = geoExtDtos[0].getLon();

        WeatherDataExtDto weatherData = weatherDataExtService.getWeatherData(lat, lon);
        Forecast3HourExtDto forecast3HourExtDto = forecast3HourExtService.getForecasts(lat, lon);

        SearchResponseDo searchResponseDo = formSearchResponseService.formSearchResponse(weatherData, forecast3HourExtDto);
        return searchResponseDo;
    }



}
