package com.example.demo.api;

import com.example.demo.domain.dto.external.WeatherDataExtDto;
import com.example.demo.service.external.impl.WeatherDataExtServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherDataApiTest {
    @Autowired
    WeatherDataExtServiceImpl weatherDataExtService;

    @GetMapping("/weather/test/{lat}/{lon}")
    public WeatherDataExtDto searchWeatherData(
            @PathVariable("lat") Double lat,
            @PathVariable("lon") Double lon
    ) {
        WeatherDataExtDto weatherData = weatherDataExtService.getWeatherData(lat, lon);
        return weatherData;
    }

}
