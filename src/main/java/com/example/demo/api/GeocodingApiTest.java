package com.example.demo.api;

import com.example.demo.domain.dto.external.GeoExtDto;
import com.example.demo.service.external.impl.GeoExtServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GeocodingApiTest {
    @Autowired
    GeoExtServiceImpl geoExtService;

    @GetMapping("/geo/test/{cityName}")
    public String geoCoding(@PathVariable("cityName") String cityName){
        GeoExtDto[] geoExtDtos = geoExtService.getGeo(cityName);
        String latAndLon = "Lat: " + geoExtDtos[0].getLat().toString() + " Lon: " + geoExtDtos[0].getLon().toString();
        return latAndLon;
    }
}
